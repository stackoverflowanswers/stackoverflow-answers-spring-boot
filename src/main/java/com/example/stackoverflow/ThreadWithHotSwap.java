package com.example.stackoverflow;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ThreadWithHotSwap {


    public ThreadWithHotSwap() {
    }

    public void start() {
        final String threadName = "THREAD_TESTE";
        final Thread thread = new Thread(getRunnable(), threadName);


        Thread.getAllStackTraces().keySet().stream()
                .filter(t -> t.getName().equals(threadName))
                .forEach(t -> System.out.println(String.format(" id: %s - name: %s - state: %s", t.getId(), t.getName(), t.getState())));


        thread.start();
    }

    private Runnable getRunnable() {
        return () -> {
            try (DatagramSocket socket = new DatagramSocket(30333, InetAddress.getByName("0.0.0.0"))) {
                socket.setBroadcast(true);
                byte[] responseBuffer = new byte[1024];
                final DatagramPacket datagramPacket = new DatagramPacket(responseBuffer, responseBuffer.length);
                String receivedResponse;

                while (true) {
                    socket.receive(datagramPacket);
                    receivedResponse = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                    System.out.println(receivedResponse);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }
}
