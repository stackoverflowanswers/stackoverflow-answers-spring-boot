# stackoverflow Answers - Spring Boot

Projeto base para teste e resoluções de duvidas do StackoverFlow sobre Spring Boot.

Perguntas respondidas:

| Pergunta  | Arquivo  |
|---|---|
| [Hot Swap do Spring apresenta “BindException: Address already in use: Cannot bind”](https://pt.stackoverflow.com/questions/404740/hot-swap-do-spring-apresenta-bindexception-address-already-in-use-cannot-bind) | [ThreadWithHotSwap.java](/src/main/java/com/example/stackoverflow/ThreadWithHotSwap.java) |